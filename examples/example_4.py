from thresult import Result, Ok, Err


@Result[float]
def f(x: float, y: float) -> float:
    z: float = x / y
    return z


r: Result = f(1, 2)
print(r)

r: Result = f(1, 0)
print(r)

try:
    with unwrap():
        r: float = f(1, 2)
        print(r)
except Exception as e:
    print('exception:', e)
    raise e

try:
    with unwrap():
        r: float = f(1, 3)
        print(r)
except Exception as e:
    print('exception:', e)
    raise e

try:
    with unwrap():
        r: float = f(1, 0)
        print(r)
except Exception as e:
    print('exception:', e)
    raise e
