import asyncio
from dataclasses import dataclass
from typing import Any, ForwardRef

import pytest

from thresult import Result, Ok, Err, ResultType, wrap_result, auto_unwrap, unwrap, WrappedBase


def test_custom_types_result_ok_err_pattern_matching():
    T: ResultType = Result[int, str]
    TOk: ResultType = Ok[int]
    TErr: ResultType = Err[str]
    a: Result[int, str] = Ok[int](10)
    b: Result[float, str] = Ok[float](20.0)
    c: Result[int, str] = Err[str]('unknown value')
    
    match a:
        case Ok(v):
            pass
        case Err(e):
            pass

    match b:
        case Ok(v):
            pass
        case Err(e):
            pass

    match c:
        case Ok(v):
            pass
        case Err(e):
            pass


def test_custom_types_result_ok_err():
    try:
        r: Result = Result()
    except TypeError as e:
        assert e


    class AOk(Ok):
        pass


    class AErr(Err):
        pass


    AResult: type = AOk[int] | AErr[str]
    assert AResult

    try:
        r: Result = AResult()
    except TypeError as e:
        assert e

    a: Ok = AOk[int](1)
    assert a
    
    b: Err = AErr[str]('some error')
    assert b


def test_custom_result_type():
    AResult: type = Ok[int] | Err[str]
    assert AResult

    try:
        r: Result = AResult()
    except TypeError as e:
        assert e

    a = Ok[int](1)
    assert a
    
    b = Err[str]('some error')
    assert b


def test_types():
    X = Ok[int]
    Y = Err[str]
    AResult: type = X.__class__.__or__(X, Y)
    assert AResult

    BResult: type = Ok[int] | Err[str]
    assert BResult

    CResult: type = Result[int, str]
    assert CResult

    assert AResult == BResult
    assert BResult == CResult
    assert AResult == CResult
    assert AResult == BResult == CResult


def test_ok_and_err_type():
    try:
        a: Result[int, str] = Ok[int]('10')
    except TypeError as e:
        assert e

    try:
        c: Result[int, str] = Err[str](10)
    except TypeError as e:
        assert e


def test_result_type_ok_or_err():
    a: Result[int, str]

    a = Ok[int](10)
    v: int = a.unwrap()

    a = Err[str]('some error')

    try:
        v: int = a.unwrap()
    except Exception as e:
        assert e


def test_wrap_result_err_unwrap():
    @wrap_result(Result[int, str])
    def f():
        a: Result[int, str]
        a = Err[str]('some error')
        v: str = a.unwrap() # panics
        return v
    
    r: Result = f()
    assert isinstance(r, Err)


def test_wrap_result_ok_unwrap():
    @wrap_result(Result[int, str])
    def f():
        a: Result[int, str]
        a = Ok[int](99)
        v: int = a.unwrap()
        return v
        
    r: Result = f()
    assert isinstance(r, Ok)


def test_result_err_unwrap():
    @Result[int, str]
    def f():
        a: Result[int, str]
        a = Err[str]('some error')
        v: int = a.unwrap()
        return v

    r: Result = f()
    assert isinstance(r, Err)


def test_result_ok_unwrap():
    @Result[int, str]
    def f():
        a: Result[int, str]
        a = Ok[int](99)
        v: int = a.unwrap()
        return v
        
    r: Result = f()
    assert isinstance(r, Ok)


def test_custom_type_ok_err_dataclass_pattern_matching():
    @dataclass
    class A:
        x: int
        y: int


    AResult: type = Ok[A] | Err[str]    
    assert AResult

    r0: AResult = Ok[A](A(10, 20))

    match r0:
        case Ok(v):
            assert isinstance(v, A)
        case _:
            raise ValueError('wrong value/type')

    match r0:
        case Ok(v):
            match v:
                case A(x, y):
                    assert isinstance(x, int) and isinstance(y, int)
                case _:
                    raise ValueError('wrong value/type')
        case _:
            raise ValueError('wrong value/type')

    match r0:
        case Ok(A(x, y)):
            assert isinstance(x, int) and isinstance(y, int)
        case _:
            raise ValueError('wrong value/type')


def test_custom_type_ok_err_class_pattern_matching():
    class A:
        __match_args__ = ('x', 'y')
        x: int
        y: int

        
        def __init__(self, x: int, y: int):
            self.x = x
            self.y = y


    AResult: type = Ok[A] | Err[str]    
    assert AResult

    r0: AResult = Ok[A](A(10, 20))

    match r0:
        case Ok(v):
            assert isinstance(v, A)
        case _:
            raise ValueError('wrong value/type')

    match r0:
        case Ok(v):
            match v:
                case A(x, y):
                    assert isinstance(x, int) and isinstance(y, int)
                case _:
                    raise ValueError('wrong value/type')
        case _:
            raise ValueError('wrong value/type')

    match r0:
        case Ok(A(x, y)):
            assert isinstance(x, int) and isinstance(y, int)
        case _:
            raise ValueError('wrong value/type')


def test_custom_result_type_ok_pattern_matching():
    class Sec:
        x: int = 10
        y: int = 20
        z: int = 30
        w: int = 40
        u: int = 50
        v: int = 60


        def __init__(self, x: int, y: int):
            self.x = x
            self.y = y


    class A(Sec):
        __match_args__ = ('x', 'y')


    AResult: type = Ok[A] | Err[str]    
    assert AResult
    # print(AResult)

    r0: AResult = Ok[A](A(10, 20))

    match r0:
        case Ok(v):
            assert isinstance(v, A)
        case _:
            raise ValueError('wrong value/type')

    match r0:
        case Ok(v):
            match v:
                case A(x, y):
                    assert isinstance(x, int) and isinstance(y, int)
                case _:
                    raise ValueError('wrong value/type')
        case _:
            raise ValueError('wrong value/type')

    match r0:
        case Ok(A(x, y)):
            assert isinstance(x, int) and isinstance(y, int)
        case _:
            raise ValueError('wrong value/type')


def test_ok_unwrap_or():
    a: Result[int, str]
    a = Ok[int](99)
    v: int = a.unwrap_or(117)
    assert v == 99


def test_err_unwrap_or():
    a: Result[int, str]
    a = Err[str]('some error')
    v: int = a.unwrap_or(117)
    assert v == 117


def test_ok_unwrap_value():
    a: Result[int, str]
    a = Ok[int](99)
    v: int = a.unwrap_value()
    assert v == 99


def test_err_unwrap_value():
    a: Result[int, str]
    a = Err[str]('some error')
    v: int = a.unwrap_value()
    assert isinstance(v, str)


@pytest.mark.asyncio
async def test_async_ok_unwrap_value():
    a: Result[int, str]
    a = Ok[int](99)
    v: int = a.unwrap_value()
    assert v == 99


@pytest.mark.asyncio
async def test_async_err_unwrap_value():
    a: Result[int, str]
    a = Err[str]('some error')
    v: int = a.unwrap_value()
    assert isinstance(v, str)


@pytest.mark.asyncio
async def test_async_err_wrap_result():
    @wrap_result(Result[int, Exception])
    async def f():
        return 1 / 0
    
    r: Result = await f()
    assert isinstance(r, Err)


@pytest.mark.asyncio
async def test_async_ok_wrap_result():
    @wrap_result(Result[int, Exception])
    async def f():
        return 1
    
    r: Result = await f()
    assert isinstance(r, Ok)


@pytest.mark.asyncio
async def test_async_err_result():
    @Result[int, Exception]
    async def f():
        return 1 / 0
    
    r: Result = await f()
    assert isinstance(r, Err)


@pytest.mark.asyncio
async def test_async_ok_result():
    @Result[int, Exception]
    async def f():
        return 1
    
    r: Result = await f()
    assert isinstance(r, Ok)


def test_sync_ok_result_ok():
    @Result[int]
    def f():
        return 1
    
    r: Result = f()
    assert isinstance(r, Ok)


def test_sync_ok_result_err():
    @Result[int]
    def f():
        raise ValueError('')
    
    r: Result = f()
    assert isinstance(r, Err)


@pytest.mark.asyncio
async def test_async_ok_result_ok():
    @Result[int]
    async def f():
        return 1
    
    r: Result = await f()
    assert isinstance(r, Ok)


@pytest.mark.asyncio
async def test_async_ok_result_err():
    @Result[int]
    async def f():
        raise ValueError('')
    
    r: Result = await f()
    assert isinstance(r, Err)


def test_result_type_context_manager_ok_result():
    @Result[float, Exception]
    def div(x: float, y: float) -> float:
        z: float = x / y
        return z

    z: float

    with div(1.0, 2.0) as z:
        assert isinstance(z, float)
        assert z == 1.0 / 2.0
    
    assert isinstance(z, float)
    assert z == 1.0 / 2.0


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ZeroDivisionError)
def test_result_type_context_manager_err_result():
    @Result[float, Exception]
    def div(x: float, y: float) -> float:
        z: float = x / y
        return z

    z: float

    with div(1.0, 0.0) as z:
        raise Exception('unreachable')


@pytest.mark.asyncio
async def test_async_ok_auto_unwrap_result():
    @auto_unwrap
    @Result[int, Exception]
    async def f():
        return 1

    r: int = await f()
    assert isinstance(r, int)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ZeroDivisionError)
async def test_async_err_auto_unwrap_result():
    @auto_unwrap
    @Result[int, Exception]
    async def f():
        return 1 / 0

    r: int = await f()
    raise Exception('unreachable')


def test_auto_unwrap_ok_result():
    @auto_unwrap
    @Result[float, Exception]
    def div(x: float, y: float) -> float:
        z: float = x / y
        return z

    r: float = div(1.0, 2.0)

    assert isinstance(r, float)
    assert r == 1.0 / 2.0


@pytest.mark.xfail(raises=ZeroDivisionError)
def test_auto_unwrap_err_result():
    @auto_unwrap
    @Result[float, Exception]
    def div(x: float, y: float) -> float:
        z: float = x / y
        return z

    z: float = div(1.0, 0.0)
    raise Exception('unreachable')


def test_auto_unwrap_type_ok_result():
    @auto_unwrap
    class A(WrappedBase):
        @Result[float, Exception]
        def div(self, x: float, y: float) -> float:
            z: float = x / y
            return z

        def mul(self, x, y) -> float:
            return x * y

    a: A = A()
    
    r: float = a.div(1.0, 2.0)
    assert isinstance(r, float)
    assert r == 1.0 / 2.0

    r: float = a.mul(2.0, 3.0)
    assert isinstance(r, float)
    assert r == 2.0 * 3.0


@pytest.mark.xfail(raises=ZeroDivisionError)
def test_auto_unwrap_type_err_result():
    @auto_unwrap
    class A(WrappedBase):
        @Result[float, Exception]
        def div(self, x: float, y: float) -> float:
            z: float = x / y
            return z

    a: A = A()
    r: float = a.div(1.0, 0.0)
    raise Exception('unreachable')


@pytest.mark.xfail(raises=ZeroDivisionError)
def test_auto_unwrap_cls_func_error():
    @auto_unwrap
    class A(WrappedBase):
        def mul(self, x, y) -> float:
            return x / y

    a: A = A()
    r: float = a.mul(2.0, 0.0)
    raise Exception('unreachable')


def test_unwrap_context():
    @Result[float, Exception]
    def f(x: float, y: float) -> float:
        z: float = x / y
        return z

    try:
        with unwrap():
            r: float = f(1, 2)
    except Exception as e:
        raise e

    assert r == 0.5


def test_unwrap_context_ok():
    @Result[float]
    def f(x: float, y: float) -> float:
        z: float = x / y
        return z


    try:
        with unwrap():
            r: float = f(1, 2)
    except Exception as e:
        raise e

    assert r == 0.5


@pytest.mark.xfail(raises=ZeroDivisionError)
def test_unwrap_context_err():
    @Result[float]
    def f(x: float, y: float) -> float:
        z: float = x / y
        return z


    try:
        with unwrap():
            r: float = f(1, 0)
    except Exception as e:
        raise e


def test_result_ok_without_types_unwrap_context_ok():
    @Result
    def f(x: float, y: float) -> float:
        z: float = x / y
        return z


    try:
        with unwrap():
            r: float = f(1, 2)
    except Exception as e:
        raise e

    assert r == 0.5


@pytest.mark.xfail(raises=ZeroDivisionError)
def test_result_err_without_types_unwrap_context_err():
    @Result
    def f(x: float, y: float) -> float:
        z: float = x / y
        return z


    try:
        with unwrap():
            r: float = f(1, 0)
    except Exception as e:
        raise e


def test_result_types():
    vs = [float, 'float', ForwardRef('float'), Any, int | float, list[int], list[int] | dict[str, int]]
    es = [float, 'float', ForwardRef('float'), Any, int | float, list[int], list[int] | dict[str, int], Exception, ValueError]

    for v in vs:
        Result[v]
    
    for v in vs:
        for e in es:
            Result[v, e]
            Ok[v]
            Err[e]
