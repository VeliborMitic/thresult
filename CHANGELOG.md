# Changelog

## 0.9.25
- Fixed: Support for `UnionType` and `GenericAlias` types for `Result`, `Ok`, `Err`.

## 0.9.24
- Fixed: Support compatible types for `Result`, `Ok`, `Err` (eg. `float, 'float', ForwardRef('float'), Any`).

## 0.9.23
- Fixed: In-place pattern-matching `Result` using `walrus operator` and `match-case` In `README.md`.

## 0.9.22
- Fixed: Better examples in `README.md` that reflects common usage of `Result` type.

## 0.9.21
- Added: Support for `Result` which infer types for `Ok` and `Err` based on annotations or defaults to `Result[Any, Exception]`.
- Added: `CHANGELOG.md`

## 0.9.20
- Fixed: Build badge.

## 0.9.19
- Fixed: Coverage badge.

## 0.9.18
- Added: `unwrap` context manager.
- Added: Tests for `unwrap` context manager.
